#include <stream9/partial_error/partial_error.hpp>

#include <stream9/errors.hpp>

#include <iostream>

namespace stream9::errors {

[[noreturn]] static void
throw_complete_error(partial_error& e)
{
    try {
        std::rethrow_if_nested(e);
    }
    catch (...) {
        std::throw_with_nested(error {
            std::move(e.what),
            std::move(e.why),
            e.context.to_string(),
            std::move(e.where),
        });
    }

    throw error {
        std::move(e.what),
        std::move(e.why),
        e.context.to_string(),
        std::move(e.where),
    };
}

[[noreturn]] static void
throw_complete_error(partial_error& e, json::object& context)
{
    for (auto&& [k, v]: context) {
        e.context[k] = std::move(v);
    }

    throw_complete_error(e);
}

[[noreturn]] void
throw_partial_error(std::error_code why,
                    std::source_location where/*= current()*/)
{
    throw_partial_error({}, std::move(why), {}, std::move(where));
}

[[noreturn]] void
throw_partial_error(std::error_code why,
                    json::object context,
                    std::source_location where/*= current()*/)
{
    throw_partial_error(
        {}, std::move(why), std::move(context), std::move(where));
}

[[noreturn]] void
throw_partial_error(std::string what,
                    std::error_code why,
                    std::source_location where/*= current()*/)
{
    throw_partial_error(
        std::move(what), std::move(why), {}, std::move(where));
}

[[noreturn]] void
throw_partial_error(std::string what,
                    std::error_code why,
                    json::object context,
                    std::source_location where/*= current()*/)
{
    if (std::current_exception()) {
        std::throw_with_nested(partial_error {
            std::move(what),
            std::move(why),
            std::move(context),
            std::move(where),
        });
    }
    else {
        throw partial_error {
            std::move(what),
            std::move(why),
            std::move(context),
            std::move(where)
        };
    }
}

[[noreturn]] void
throw_error(std::error_code why,
            std::source_location loc/*= current()*/)
{
    throw_error({}, std::move(why), {}, std::move(loc));
}

[[noreturn]] void
throw_error(std::error_code why,
            json::object cxt,
            std::source_location loc/*= current()*/)
{
    throw_error({}, std::move(why), std::move(cxt), std::move(loc));
}

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            std::source_location loc/*= current()*/)
{
    throw_error(std::move(what), std::move(why), {}, std::move(loc));
}

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            json::object cxt,
            std::source_location loc/*= current()*/)
{
    if (std::current_exception()) {
        try {
            throw;
        }
        catch (partial_error& e) {
            throw_complete_error(e, cxt);
        }
        catch (...) {
            std::throw_with_nested(error {
                std::move(what),
                std::move(why),
                cxt.to_string(),
                std::move(loc),
            });
        }
    }
    else {
        throw error {
            std::move(what),
            std::move(why),
            cxt.to_string(),
            std::move(loc),
        };
    }
}

[[noreturn]] void
rethrow_error(std::error_category const& category,
              std::source_location loc/*= current()*/)
{
    assert(std::current_exception());

    try {
        throw;
    }
    catch (partial_error& e) {
        throw_complete_error(e);
    }
    catch (...) {
        auto const& ec = current_error_code();
        if (ec.category() == category) {
            std::throw_with_nested(error {
                ec,
                std::move(loc),
            });
        }
        else {
            std::throw_with_nested(error {
                errc::internal_error,
                std::move(loc),
            });
        }
    }
}

[[noreturn]] void
rethrow_error(std::error_category const& category,
              json::object cxt,
              std::source_location loc/*= current()*/)
{
    assert(std::current_exception());

    try {
        throw;
    }
    catch (partial_error& e) {
        throw_complete_error(e, cxt);
    }
    catch (...) {
        auto const& ec = current_error_code();
        if (ec.category() == category) {
            std::throw_with_nested(error {
                ec,
                cxt.to_string(),
                std::move(loc),
            });
        }
        else {
            std::throw_with_nested(error {
                errc::internal_error,
                cxt.to_string(),
                std::move(loc),
            });
        }
    }
}

} // namespace stream9::errors
