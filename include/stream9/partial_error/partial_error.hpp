#ifndef STREAM9_PARTIAL_ERROR_HPP
#define STREAM9_PARTIAL_ERROR_HPP

#include <stream9/errors.hpp>

#include <exception>
#include <source_location>
#include <string>
#include <system_error>

#include <stream9/json.hpp>

namespace stream9::errors {

struct partial_error
{
    std::string what;
    std::error_code why;
    json::object context;
    std::source_location where;
};

[[noreturn]] void
throw_partial_error(std::error_code why,
                    std::source_location = std::source_location::current());

[[noreturn]] void
throw_partial_error(std::error_code why,
                    json::object context,
                    std::source_location = std::source_location::current());

[[noreturn]] void
throw_partial_error(std::string what,
                    std::error_code why,
                    std::source_location = std::source_location::current());

[[noreturn]] void
throw_partial_error(std::string what,
                    std::error_code why,
                    json::object context,
                    std::source_location = std::source_location::current());

[[noreturn]] void
throw_error(std::error_code why,
            std::source_location = std::source_location::current());

[[noreturn]] void
throw_error(std::error_code why,
            json::object context,
            std::source_location = std::source_location::current());

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            std::source_location = std::source_location::current());

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            json::object context,
            std::source_location = std::source_location::current());

[[noreturn]] void
rethrow_error(std::error_category const& current_category,
              std::source_location loc = std::source_location::current());

[[noreturn]] void
rethrow_error(std::error_category const& current_category,
              json::object context,
              std::source_location loc = std::source_location::current());

} // namespace stream9::errors

#endif // STREAM9_PARTIAL_ERROR_HPP
